﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace AntiVirus
{
    public partial class Form1 : Form
    {
        work_File F2 = new work_File();
        Node_Write NW = new Node_Write();

        public Form1()
        {
            InitializeComponent();
            this.BackColor = Color.FromArgb(41, 44, 51); 
            this.MouseDown += new MouseEventHandler(Form1_MouseDown);
        }

        //Движение окна без рамки
        void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            base.Capture = false;
            Message m = Message.Create(base.Handle, 0xa1, new IntPtr(2), IntPtr.Zero);
            this.WndProc(ref m);
        }

        // Кнопка закрытия окна
        private void Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        // Добавить сигнатуру в базу
        private void addSign_Click(object sender, EventArgs e)
        {
            addSign addSign = new addSign();
            addSign.Show();
        }

        // Вывод списка сигнатур
        private void Basesign_Click(object sender, EventArgs e)
        {     
            NW.Signatures_List(listBox1, files);          
        }
    }
}

