﻿namespace AntiVirus
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Basesign = new System.Windows.Forms.Button();
            this.addSign = new System.Windows.Forms.Button();
            this.files = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.DirScan = new System.Windows.Forms.Button();
            this.signPath = new System.Windows.Forms.TextBox();
            this.changeSignPath = new System.Windows.Forms.Button();
            this.scanir = new System.Windows.Forms.Button();
            this.scanPath = new System.Windows.Forms.TextBox();
            this.home = new System.Windows.Forms.Button();
            this.Inform = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelleft = new System.Windows.Forms.Panel();
            this.main_scan = new System.Windows.Forms.Button();
            this.tracking = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.deleteFile = new System.Windows.Forms.Button();
            this.deleteInfFile = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Close = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel_scan = new System.Windows.Forms.Panel();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.scan_label_2 = new System.Windows.Forms.Label();
            this.scan_listBox = new System.Windows.Forms.ListBox();
            this.scan_label = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.file_watcher_path = new System.Windows.Forms.Button();
            this.file_watcher_textbox = new System.Windows.Forms.TextBox();
            this.tracking_panel = new System.Windows.Forms.Panel();
            this.file_watcher_label = new System.Windows.Forms.Label();
            this.file_watcher_stop = new System.Windows.Forms.Button();
            this.file_watcher_clear = new System.Windows.Forms.Button();
            this.file_watcher_start = new System.Windows.Forms.Button();
            this.file_watcher_checkBox = new System.Windows.Forms.CheckBox();
            this.file_watcher_listBox = new System.Windows.Forms.ListBox();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.bunifuCircleProgressbar1 = new Bunifu.Framework.UI.BunifuCircleProgressbar();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel_scan.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tracking_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // Basesign
            // 
            this.Basesign.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(62)))), ((int)(((byte)(74)))));
            this.Basesign.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.Basesign.FlatAppearance.BorderSize = 0;
            this.Basesign.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Basesign.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Basesign.ForeColor = System.Drawing.Color.White;
            this.Basesign.Location = new System.Drawing.Point(307, 491);
            this.Basesign.Margin = new System.Windows.Forms.Padding(4);
            this.Basesign.Name = "Basesign";
            this.Basesign.Size = new System.Drawing.Size(257, 80);
            this.Basesign.TabIndex = 10;
            this.Basesign.Text = "Показать базу вирусов";
            this.Basesign.UseVisualStyleBackColor = false;
            this.Basesign.Click += new System.EventHandler(this.Basesign_Click);
            // 
            // addSign
            // 
            this.addSign.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(62)))), ((int)(((byte)(74)))));
            this.addSign.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.addSign.FlatAppearance.BorderSize = 0;
            this.addSign.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addSign.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.addSign.ForeColor = System.Drawing.Color.White;
            this.addSign.Location = new System.Drawing.Point(42, 491);
            this.addSign.Margin = new System.Windows.Forms.Padding(4);
            this.addSign.Name = "addSign";
            this.addSign.Size = new System.Drawing.Size(257, 80);
            this.addSign.TabIndex = 8;
            this.addSign.Text = "Добавить сигнатуру";
            this.addSign.UseVisualStyleBackColor = false;
            this.addSign.Click += new System.EventHandler(this.addSign_Click);
            // 
            // files
            // 
            this.files.AutoSize = true;
            this.files.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.files.ForeColor = System.Drawing.Color.White;
            this.files.Location = new System.Drawing.Point(324, 458);
            this.files.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.files.Name = "files";
            this.files.Size = new System.Drawing.Size(0, 23);
            this.files.TabIndex = 5;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.HorizontalScrollbar = true;
            this.listBox1.ItemHeight = 20;
            this.listBox1.Location = new System.Drawing.Point(10, 130);
            this.listBox1.Margin = new System.Windows.Forms.Padding(4);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(876, 324);
            this.listBox1.TabIndex = 1;
            // 
            // DirScan
            // 
            this.DirScan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(62)))), ((int)(((byte)(74)))));
            this.DirScan.FlatAppearance.BorderSize = 0;
            this.DirScan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DirScan.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DirScan.ForeColor = System.Drawing.Color.White;
            this.DirScan.Location = new System.Drawing.Point(7, 23);
            this.DirScan.Margin = new System.Windows.Forms.Padding(4);
            this.DirScan.Name = "DirScan";
            this.DirScan.Size = new System.Drawing.Size(238, 75);
            this.DirScan.TabIndex = 12;
            this.DirScan.Text = "Изменить путь сканирования";
            this.DirScan.UseVisualStyleBackColor = false;
            this.DirScan.Click += new System.EventHandler(this.DirScan_Click);
            // 
            // signPath
            // 
            this.signPath.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.signPath.Location = new System.Drawing.Point(253, 27);
            this.signPath.Margin = new System.Windows.Forms.Padding(4);
            this.signPath.Multiline = true;
            this.signPath.Name = "signPath";
            this.signPath.Size = new System.Drawing.Size(616, 75);
            this.signPath.TabIndex = 0;
            // 
            // changeSignPath
            // 
            this.changeSignPath.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(62)))), ((int)(((byte)(74)))));
            this.changeSignPath.FlatAppearance.BorderSize = 0;
            this.changeSignPath.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.changeSignPath.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.changeSignPath.ForeColor = System.Drawing.Color.White;
            this.changeSignPath.Location = new System.Drawing.Point(7, 27);
            this.changeSignPath.Margin = new System.Windows.Forms.Padding(4);
            this.changeSignPath.Name = "changeSignPath";
            this.changeSignPath.Size = new System.Drawing.Size(238, 75);
            this.changeSignPath.TabIndex = 6;
            this.changeSignPath.Text = "Изменить путь к файлу базы вирусов";
            this.changeSignPath.UseVisualStyleBackColor = false;
            this.changeSignPath.Click += new System.EventHandler(this.changeSignPath_Click);
            // 
            // scanir
            // 
            this.scanir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(62)))), ((int)(((byte)(74)))));
            this.scanir.FlatAppearance.BorderSize = 0;
            this.scanir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.scanir.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.scanir.ForeColor = System.Drawing.Color.White;
            this.scanir.Location = new System.Drawing.Point(644, 397);
            this.scanir.Margin = new System.Windows.Forms.Padding(4);
            this.scanir.Name = "scanir";
            this.scanir.Size = new System.Drawing.Size(238, 77);
            this.scanir.TabIndex = 7;
            this.scanir.Text = "Сканировать";
            this.scanir.UseVisualStyleBackColor = false;
            this.scanir.Click += new System.EventHandler(this.scanir_Click_1);
            // 
            // scanPath
            // 
            this.scanPath.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.scanPath.Location = new System.Drawing.Point(253, 23);
            this.scanPath.Margin = new System.Windows.Forms.Padding(4);
            this.scanPath.Multiline = true;
            this.scanPath.Name = "scanPath";
            this.scanPath.Size = new System.Drawing.Size(623, 75);
            this.scanPath.TabIndex = 0;
            // 
            // home
            // 
            this.home.FlatAppearance.BorderSize = 0;
            this.home.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.home.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.home.ForeColor = System.Drawing.Color.White;
            this.home.Location = new System.Drawing.Point(4, 10);
            this.home.Name = "home";
            this.home.Size = new System.Drawing.Size(173, 82);
            this.home.TabIndex = 1;
            this.home.Text = "Home";
            this.home.UseVisualStyleBackColor = true;
            this.home.Click += new System.EventHandler(this.home_Click);
            // 
            // Inform
            // 
            this.Inform.FlatAppearance.BorderSize = 0;
            this.Inform.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Inform.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Inform.ForeColor = System.Drawing.Color.White;
            this.Inform.Location = new System.Drawing.Point(4, 309);
            this.Inform.Name = "Inform";
            this.Inform.Size = new System.Drawing.Size(173, 82);
            this.Inform.TabIndex = 2;
            this.Inform.Text = "Сигнатуры";
            this.Inform.UseVisualStyleBackColor = true;
            this.Inform.Click += new System.EventHandler(this.Inform_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panelleft);
            this.panel1.Controls.Add(this.main_scan);
            this.panel1.Controls.Add(this.tracking);
            this.panel1.Controls.Add(this.home);
            this.panel1.Controls.Add(this.Inform);
            this.panel1.Location = new System.Drawing.Point(12, 48);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(180, 590);
            this.panel1.TabIndex = 13;
            // 
            // panelleft
            // 
            this.panelleft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.panelleft.Location = new System.Drawing.Point(168, 112);
            this.panelleft.Name = "panelleft";
            this.panelleft.Size = new System.Drawing.Size(10, 82);
            this.panelleft.TabIndex = 3;
            // 
            // main_scan
            // 
            this.main_scan.FlatAppearance.BorderSize = 0;
            this.main_scan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.main_scan.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.main_scan.ForeColor = System.Drawing.Color.White;
            this.main_scan.Location = new System.Drawing.Point(4, 112);
            this.main_scan.Name = "main_scan";
            this.main_scan.Size = new System.Drawing.Size(173, 82);
            this.main_scan.TabIndex = 18;
            this.main_scan.Text = "Сканирование";
            this.main_scan.UseVisualStyleBackColor = true;
            this.main_scan.Click += new System.EventHandler(this.main_scan_Click);
            // 
            // tracking
            // 
            this.tracking.FlatAppearance.BorderSize = 0;
            this.tracking.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tracking.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tracking.ForeColor = System.Drawing.Color.White;
            this.tracking.Location = new System.Drawing.Point(4, 200);
            this.tracking.Name = "tracking";
            this.tracking.Size = new System.Drawing.Size(173, 82);
            this.tracking.TabIndex = 4;
            this.tracking.Text = "FileWatcher";
            this.tracking.UseVisualStyleBackColor = true;
            this.tracking.Click += new System.EventHandler(this.tracking_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Location = new System.Drawing.Point(199, 48);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(901, 590);
            this.panel2.TabIndex = 14;
            // 
            // deleteFile
            // 
            this.deleteFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(62)))), ((int)(((byte)(74)))));
            this.deleteFile.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.deleteFile.FlatAppearance.BorderSize = 0;
            this.deleteFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deleteFile.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.deleteFile.ForeColor = System.Drawing.Color.White;
            this.deleteFile.Location = new System.Drawing.Point(6, 397);
            this.deleteFile.Margin = new System.Windows.Forms.Padding(4);
            this.deleteFile.Name = "deleteFile";
            this.deleteFile.Size = new System.Drawing.Size(257, 77);
            this.deleteFile.TabIndex = 9;
            this.deleteFile.Text = "Удалить зараженный файл";
            this.deleteFile.UseVisualStyleBackColor = false;
            this.deleteFile.Click += new System.EventHandler(this.deleteFile_Click_2);
            // 
            // deleteInfFile
            // 
            this.deleteInfFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(62)))), ((int)(((byte)(74)))));
            this.deleteInfFile.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.deleteInfFile.FlatAppearance.BorderSize = 0;
            this.deleteInfFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deleteInfFile.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.deleteInfFile.ForeColor = System.Drawing.Color.White;
            this.deleteInfFile.Location = new System.Drawing.Point(572, 491);
            this.deleteInfFile.Margin = new System.Windows.Forms.Padding(4);
            this.deleteInfFile.Name = "deleteInfFile";
            this.deleteInfFile.Size = new System.Drawing.Size(257, 80);
            this.deleteInfFile.TabIndex = 11;
            this.deleteInfFile.Text = "Удалить сигнатуру вируса";
            this.deleteInfFile.UseVisualStyleBackColor = false;
            this.deleteInfFile.Click += new System.EventHandler(this.deleteInfFile_Click);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.files);
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Controls.Add(this.listBox1);
            this.panel3.Controls.Add(this.addSign);
            this.panel3.Controls.Add(this.Basesign);
            this.panel3.Controls.Add(this.deleteInfFile);
            this.panel3.Location = new System.Drawing.Point(199, 48);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(901, 590);
            this.panel3.TabIndex = 15;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.changeSignPath);
            this.groupBox1.Controls.Add(this.signPath);
            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(10, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(876, 111);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Путь к базе сигнатур";
            // 
            // Close
            // 
            this.Close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Close.FlatAppearance.BorderSize = 0;
            this.Close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Close.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Close.ForeColor = System.Drawing.Color.White;
            this.Close.Location = new System.Drawing.Point(1058, 1);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(48, 43);
            this.Close.TabIndex = 16;
            this.Close.Text = "X";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(457, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(241, 30);
            this.label1.TabIndex = 17;
            this.label1.Text = "Антивирус v.0.4.2.4";
            // 
            // panel_scan
            // 
            this.panel_scan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_scan.Controls.Add(this.bunifuCircleProgressbar1);
            this.panel_scan.Controls.Add(this.progressBar1);
            this.panel_scan.Controls.Add(this.scan_label_2);
            this.panel_scan.Controls.Add(this.deleteFile);
            this.panel_scan.Controls.Add(this.scan_listBox);
            this.panel_scan.Controls.Add(this.scan_label);
            this.panel_scan.Controls.Add(this.groupBox2);
            this.panel_scan.Controls.Add(this.scanir);
            this.panel_scan.Location = new System.Drawing.Point(199, 48);
            this.panel_scan.Name = "panel_scan";
            this.panel_scan.Size = new System.Drawing.Size(901, 590);
            this.panel_scan.TabIndex = 18;
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.Color.Yellow;
            this.progressBar1.ForeColor = System.Drawing.Color.Black;
            this.progressBar1.Location = new System.Drawing.Point(6, 369);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(876, 23);
            this.progressBar1.TabIndex = 19;
            // 
            // scan_label_2
            // 
            this.scan_label_2.AutoSize = true;
            this.scan_label_2.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.scan_label_2.ForeColor = System.Drawing.Color.White;
            this.scan_label_2.Location = new System.Drawing.Point(303, 407);
            this.scan_label_2.Name = "scan_label_2";
            this.scan_label_2.Size = new System.Drawing.Size(307, 23);
            this.scan_label_2.TabIndex = 17;
            this.scan_label_2.Text = "Время сканирования 00:00:00\r\n";
            // 
            // scan_listBox
            // 
            this.scan_listBox.ItemHeight = 20;
            this.scan_listBox.Location = new System.Drawing.Point(6, 149);
            this.scan_listBox.Name = "scan_listBox";
            this.scan_listBox.Size = new System.Drawing.Size(876, 204);
            this.scan_listBox.TabIndex = 20;
            // 
            // scan_label
            // 
            this.scan_label.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scan_label.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.scan_label.ForeColor = System.Drawing.Color.White;
            this.scan_label.Location = new System.Drawing.Point(8, 5);
            this.scan_label.Name = "scan_label";
            this.scan_label.Size = new System.Drawing.Size(889, 23);
            this.scan_label.TabIndex = 15;
            this.scan_label.Text = "Дата";
            this.scan_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.scanPath);
            this.groupBox2.Controls.Add(this.DirScan);
            this.groupBox2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(6, 31);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(889, 112);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Новый путь сканирования";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // backgroundWorker2
            // 
            this.backgroundWorker2.WorkerReportsProgress = true;
            this.backgroundWorker2.WorkerSupportsCancellation = true;
            this.backgroundWorker2.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker2_DoWork);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.file_watcher_path);
            this.groupBox3.Controls.Add(this.file_watcher_textbox);
            this.groupBox3.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.groupBox3.ForeColor = System.Drawing.Color.White;
            this.groupBox3.Location = new System.Drawing.Point(8, 11);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(880, 111);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Путь к директории для наблюдения";
            // 
            // file_watcher_path
            // 
            this.file_watcher_path.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(62)))), ((int)(((byte)(74)))));
            this.file_watcher_path.FlatAppearance.BorderSize = 0;
            this.file_watcher_path.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.file_watcher_path.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.file_watcher_path.ForeColor = System.Drawing.Color.White;
            this.file_watcher_path.Location = new System.Drawing.Point(7, 27);
            this.file_watcher_path.Margin = new System.Windows.Forms.Padding(4);
            this.file_watcher_path.Name = "file_watcher_path";
            this.file_watcher_path.Size = new System.Drawing.Size(238, 75);
            this.file_watcher_path.TabIndex = 6;
            this.file_watcher_path.Text = "Путь к директории для наблюдения";
            this.file_watcher_path.UseVisualStyleBackColor = false;
            this.file_watcher_path.Click += new System.EventHandler(this.file_watcher_path_Click);
            // 
            // file_watcher_textbox
            // 
            this.file_watcher_textbox.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.file_watcher_textbox.Location = new System.Drawing.Point(253, 27);
            this.file_watcher_textbox.Margin = new System.Windows.Forms.Padding(4);
            this.file_watcher_textbox.Multiline = true;
            this.file_watcher_textbox.Name = "file_watcher_textbox";
            this.file_watcher_textbox.Size = new System.Drawing.Size(622, 75);
            this.file_watcher_textbox.TabIndex = 0;
            // 
            // tracking_panel
            // 
            this.tracking_panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tracking_panel.Controls.Add(this.file_watcher_label);
            this.tracking_panel.Controls.Add(this.file_watcher_stop);
            this.tracking_panel.Controls.Add(this.file_watcher_clear);
            this.tracking_panel.Controls.Add(this.file_watcher_start);
            this.tracking_panel.Controls.Add(this.file_watcher_checkBox);
            this.tracking_panel.Controls.Add(this.file_watcher_listBox);
            this.tracking_panel.Controls.Add(this.groupBox3);
            this.tracking_panel.Location = new System.Drawing.Point(199, 48);
            this.tracking_panel.Name = "tracking_panel";
            this.tracking_panel.Size = new System.Drawing.Size(901, 590);
            this.tracking_panel.TabIndex = 19;
            // 
            // file_watcher_label
            // 
            this.file_watcher_label.AutoSize = true;
            this.file_watcher_label.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.file_watcher_label.ForeColor = System.Drawing.Color.White;
            this.file_watcher_label.Location = new System.Drawing.Point(454, 129);
            this.file_watcher_label.Name = "file_watcher_label";
            this.file_watcher_label.Size = new System.Drawing.Size(389, 23);
            this.file_watcher_label.TabIndex = 18;
            this.file_watcher_label.Text = "Кол-во файлов в данной директории:\r\n";
            // 
            // file_watcher_stop
            // 
            this.file_watcher_stop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(62)))), ((int)(((byte)(74)))));
            this.file_watcher_stop.FlatAppearance.BorderSize = 0;
            this.file_watcher_stop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.file_watcher_stop.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.file_watcher_stop.ForeColor = System.Drawing.Color.White;
            this.file_watcher_stop.Location = new System.Drawing.Point(581, 409);
            this.file_watcher_stop.Name = "file_watcher_stop";
            this.file_watcher_stop.Size = new System.Drawing.Size(238, 75);
            this.file_watcher_stop.TabIndex = 17;
            this.file_watcher_stop.Text = "Прекратить слежение\r\n";
            this.file_watcher_stop.UseVisualStyleBackColor = false;
            this.file_watcher_stop.Click += new System.EventHandler(this.file_watcher_stop_Click);
            // 
            // file_watcher_clear
            // 
            this.file_watcher_clear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(62)))), ((int)(((byte)(74)))));
            this.file_watcher_clear.FlatAppearance.BorderSize = 0;
            this.file_watcher_clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.file_watcher_clear.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.file_watcher_clear.ForeColor = System.Drawing.Color.White;
            this.file_watcher_clear.Location = new System.Drawing.Point(337, 409);
            this.file_watcher_clear.Name = "file_watcher_clear";
            this.file_watcher_clear.Size = new System.Drawing.Size(238, 75);
            this.file_watcher_clear.TabIndex = 16;
            this.file_watcher_clear.Text = "Очистить окно";
            this.file_watcher_clear.UseVisualStyleBackColor = false;
            this.file_watcher_clear.Click += new System.EventHandler(this.file_watcher_clear_Click);
            // 
            // file_watcher_start
            // 
            this.file_watcher_start.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(62)))), ((int)(((byte)(74)))));
            this.file_watcher_start.FlatAppearance.BorderSize = 0;
            this.file_watcher_start.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.file_watcher_start.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.file_watcher_start.ForeColor = System.Drawing.Color.White;
            this.file_watcher_start.Location = new System.Drawing.Point(93, 409);
            this.file_watcher_start.Name = "file_watcher_start";
            this.file_watcher_start.Size = new System.Drawing.Size(238, 75);
            this.file_watcher_start.TabIndex = 16;
            this.file_watcher_start.Text = "Начать слежение";
            this.file_watcher_start.UseVisualStyleBackColor = false;
            this.file_watcher_start.Click += new System.EventHandler(this.file_watcher_start_Click);
            // 
            // file_watcher_checkBox
            // 
            this.file_watcher_checkBox.AutoSize = true;
            this.file_watcher_checkBox.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.file_watcher_checkBox.ForeColor = System.Drawing.Color.White;
            this.file_watcher_checkBox.Location = new System.Drawing.Point(8, 128);
            this.file_watcher_checkBox.Name = "file_watcher_checkBox";
            this.file_watcher_checkBox.Size = new System.Drawing.Size(443, 27);
            this.file_watcher_checkBox.TabIndex = 15;
            this.file_watcher_checkBox.Text = "Отслеживать сканируемую директорию";
            this.file_watcher_checkBox.UseVisualStyleBackColor = true;
            // 
            // file_watcher_listBox
            // 
            this.file_watcher_listBox.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.file_watcher_listBox.FormattingEnabled = true;
            this.file_watcher_listBox.HorizontalScrollbar = true;
            this.file_watcher_listBox.ItemHeight = 23;
            this.file_watcher_listBox.Location = new System.Drawing.Point(8, 161);
            this.file_watcher_listBox.Name = "file_watcher_listBox";
            this.file_watcher_listBox.Size = new System.Drawing.Size(880, 234);
            this.file_watcher_listBox.TabIndex = 14;
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 30;
            this.bunifuElipse1.TargetControl = this;
            // 
            // bunifuCircleProgressbar1
            // 
            this.bunifuCircleProgressbar1.animated = true;
            this.bunifuCircleProgressbar1.animationIterval = 5;
            this.bunifuCircleProgressbar1.animationSpeed = 300;
            this.bunifuCircleProgressbar1.AutoSize = true;
            this.bunifuCircleProgressbar1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCircleProgressbar1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuCircleProgressbar1.BackgroundImage")));
            this.bunifuCircleProgressbar1.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bunifuCircleProgressbar1.ForeColor = System.Drawing.Color.SeaGreen;
            this.bunifuCircleProgressbar1.LabelVisible = true;
            this.bunifuCircleProgressbar1.LineProgressThickness = 8;
            this.bunifuCircleProgressbar1.LineThickness = 5;
            this.bunifuCircleProgressbar1.Location = new System.Drawing.Point(378, 437);
            this.bunifuCircleProgressbar1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bunifuCircleProgressbar1.MaxValue = 100;
            this.bunifuCircleProgressbar1.Name = "bunifuCircleProgressbar1";
            this.bunifuCircleProgressbar1.ProgressBackColor = System.Drawing.Color.Gainsboro;
            this.bunifuCircleProgressbar1.ProgressColor = System.Drawing.Color.SeaGreen;
            this.bunifuCircleProgressbar1.Size = new System.Drawing.Size(120, 120);
            this.bunifuCircleProgressbar1.TabIndex = 21;
            this.bunifuCircleProgressbar1.Value = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.ClientSize = new System.Drawing.Size(1113, 650);
            this.Controls.Add(this.tracking_panel);
            this.Controls.Add(this.panel_scan);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Close);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1113, 650);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel_scan.ResumeLayout(false);
            this.panel_scan.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tracking_panel.ResumeLayout(false);
            this.tracking_panel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox scanPath;
        private System.Windows.Forms.TextBox signPath;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button scanir_Click;
        private System.Windows.Forms.Button deleteFile_Click;
        private System.Windows.Forms.Label files;
        private System.Windows.Forms.Button changeSignPath;
        private System.Windows.Forms.Button scanir;
        private System.Windows.Forms.Button addSign;
        private System.Windows.Forms.Button Basesign;
        private System.Windows.Forms.Button DirScan;
        private System.Windows.Forms.Button home;
        private System.Windows.Forms.Button Inform;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button Close;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelleft;
        private System.Windows.Forms.Button tracking;
        private System.Windows.Forms.Button deleteFile;
        private System.Windows.Forms.Button deleteInfFile;
        private System.Windows.Forms.Button main_scan;
        private System.Windows.Forms.Panel panel_scan;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label scan_label;
        private System.Windows.Forms.Label scan_label_2;
        public System.Windows.Forms.ProgressBar progressBar1;
        public System.ComponentModel.BackgroundWorker backgroundWorker1;
        public System.ComponentModel.BackgroundWorker backgroundWorker2;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.ListBox scan_listBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button file_watcher_path;
        private System.Windows.Forms.TextBox file_watcher_textbox;
        private System.Windows.Forms.Panel tracking_panel;
        public System.Windows.Forms.CheckBox file_watcher_checkBox;
        private System.Windows.Forms.ListBox file_watcher_listBox;
        private System.Windows.Forms.Button file_watcher_stop;
        private System.Windows.Forms.Button file_watcher_start;
        private System.Windows.Forms.Label file_watcher_label;
        private System.Windows.Forms.Button file_watcher_clear;
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuCircleProgressbar bunifuCircleProgressbar1;
    }
    
}

