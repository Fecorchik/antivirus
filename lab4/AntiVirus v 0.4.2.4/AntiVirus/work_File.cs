﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Threading;


namespace AntiVirus
{
    public partial class work_File : ISubject
    { 
            // --------------------------------Наблюдатель------------------------------------------------------
            public List<string> Observer_str = new List<string>();
            public string Observer_str2 {get;set;}
            //public int detected_virus { get; set; }
            private List<IObserver> _observers = new List<IObserver>();

            // Методы управления подпиской.
            public void Attach(IObserver observer)
            {
                Console.WriteLine("Антивирус: Добавлен наблюдатель.");
                this._observers.Add(observer);
            }
             public void Detach(IObserver observer)
            {
                this._observers.Remove(observer);
                Console.WriteLine("Антивирус: Удаление наблюдателя.");
            }
            public void Notify()
            {
                Console.WriteLine("Антивирус: Уведомление наблюдателей...");

                foreach (var observer in _observers)
                {
                    observer.Update(this);
                }
            }

         // --------------------------------Наблюдатель------------------------------------------------------

            public string Path_Last_Deleted { get; set; }
            bool proverka = false;

        /// <summary>
        /// Указать путь к файлу
        /// </summary>
        /// <returns>Путь к файлу</returns>
        public string pathScan()
        {
            String line = "";
            FolderBrowserDialog FBD = new FolderBrowserDialog();
            if (FBD.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    StreamWriter sw = new StreamWriter("path.txt");
                    sw.WriteLine(FBD.SelectedPath);
                    sw.Close();
                }
                catch (Exception err)
                {
                    MessageBox.Show("Exception: " + err.Message);
                }

                line = FBD.SelectedPath;
            }
            return line;
        }

        /// <summary>
        /// Получить список сигнатур
        /// </summary>
        /// <returns>Список сигнатур</returns>
        public List<Node_Write.Virus> getVirusDB()
        {
            Form1 f1 = new Form1();
            BinaryFormatter binForm = new BinaryFormatter();

            // узнать путь к файлу
            StreamReader sr = new StreamReader("sign.txt");
            string line = sr.ReadLine();
            sr.Close();

            FileStream virusDataBase = new FileStream(line, FileMode.Open);
            List<Node_Write.Virus> dataBase = new List<Node_Write.Virus>();
            while (virusDataBase.Position < virusDataBase.Length)
            {
                dataBase.Add((Node_Write.Virus)binForm.Deserialize(virusDataBase));    
            }
            virusDataBase.Close();
            return dataBase;
        }



       
        /// <summary>
        /// Сканировать директорию
        /// </summary>
        /// <param name="path">Путь к директории</param>
        /// <param name="sign">База сигнатур</param>
       //public List<string> scan(string path)
        public List<string> scan_List(string path)
        {
            // создаем АВЛ дерево
            List<Node_Write.Virus> sign = getVirusDB();
            Balanced_binary_tree_search tree = new Balanced_binary_tree_search();

            // создаем АВЛ дерево из сигнатур
            for (int i = 0; i < sign.Count; i++)
            {
                tree.Add(sign[i]);
            }

            addSign aS = new addSign();
            List<string> str = new List<string>();

            DirectoryInfo dir = new DirectoryInfo(path);

            foreach (string f in Directory.GetFiles(path, "*.*", SearchOption.AllDirectories).Union(Directory.GetFiles(path, "*.dll", SearchOption.AllDirectories)))
            {

                if (!Program.fm1.backgroundWorker1.CancellationPending)
                {
                Node_Write.Virus? virus;
                string signHash, signHash2;

               Program.fm1.progressbar_vivod();

                //if (f == Path_Last_Deleted)
                //{
                //    Console.WriteLine("Найдена ошибка. Похоже файл удален. Пропускаю его");
                //    continue;
                //}        
                foreach (string f2 in Directory.GetFiles(path, "*.*", SearchOption.AllDirectories).Union(Directory.GetFiles(path, "*.dll", SearchOption.AllDirectories)))
                {
                    if (f == f2)
                        proverka = true;
                }
                if (!proverka)
                {
                    Console.WriteLine("Похоже файл удален. Пропускаю его");
                    proverka = false;
                    continue;
                }
                proverka = false;
                
                // Открываем файл
                FileStream file = new FileStream(f, FileMode.Open);
                // узнаем, где у него исп секция
                int[] data = aS.exeSect(file);
                // смещаем его на начало исп секции
                file.Seek(data[1], SeekOrigin.Begin);

                // прогоняем для всей исп области, смещаясь
                // каждый раз на 1 байт и беря 8 байт как длину
                // возможного вируса
                while (file.Position + 8 < data[1] + data[0])
                {
                    // получаем блок кода из сканируемого файла
                    // на месте возможного вируса длиной 8 байт
                    signHash = aS.read(file, 8);
                    // хешируем его
                    signHash = aS.hash(signHash);
                    // сравниваем с помощью АВЛ по хешу
                    // если возвращаемое значение не null, то
                    // это, возможно, зараженный файл и нужно
                    // проверить полный хеш
                    virus = tree.Find(signHash);
                    if (virus != null)
                    {
                        // возвращаемся назад на 8 байт
                        file.Seek(-8, SeekOrigin.Current);
                        // читаем длину сигнатуры
                        signHash2 = aS.read(file, ((Node_Write.Virus)virus).length);
                        signHash2 = aS.hash(signHash2);
                        // если полные хеши равны, то это точно зараженный файл
                        if (signHash2 == ((Node_Write.Virus)virus).hash)
                        {
                            Console.WriteLine("\nАнтивирус: Обнаруженена угроза.");
                            this.Observer_str.Add(f);
                            Observer_str2 = f;
                            this.Notify();
                           
                            // добавляем в str путь к файлу
                            str.Add(f);
                            // ищем сигнатуру - источник заражения и
                            // прибавляем ей число обнаружений
                            for (int i = 0; i < sign.Count; i++)
                            {
                                if (sign[i].hash == ((Node_Write.Virus)virus).hash)
                                {
                                    //так делается, ибо это ошибка компилятора
                                    Node_Write.Virus vir = sign[i];
                                    vir.detect++;
                                    sign[i] = vir;
                                }
                            }
                            break;
                        }
                        // отменяем смещения
                        file.Seek(8 - ((Node_Write.Virus)virus).length, SeekOrigin.Current);
                    }
                    // идем назад на 8 байт и смещаем на 1 байт
                    file.Seek(-8 + 1, SeekOrigin.Current);
                }
                file.Close();

                }
            }

            // Путь к файлу базы сигнатур
            StreamReader sr = new StreamReader("sign.txt");
            string line = sr.ReadLine();
            sr.Close();

            // сериализуем структуру (превращаем в бинарную форму)
            FileStream VDB = new FileStream(line, FileMode.Create, FileAccess.Write);
            BinaryFormatter binForm = new BinaryFormatter();
            // добавляем в VDB (файл базы сигнатур) объект sign
            // оно добавится туда в 2ой форме и сложно будет что-то понять
            // чтобы что-то понять, надо будет десериализовывать

           

            for (int i = 0; i < sign.Count; i++)
            {
                binForm.Serialize(VDB, sign[i]);
            }
            VDB.Close();

            return str;
        }

        public bool scan_String(string path, string f)
        {
            // создаем АВЛ дерево
            List<Node_Write.Virus> sign = getVirusDB();
            Balanced_binary_tree_search tree = new Balanced_binary_tree_search();

            // создаем АВЛ дерево из сигнатур
            for (int i = 0; i < sign.Count; i++)
            {
                tree.Add(sign[i]);
            }

            addSign aS = new addSign();
            bool B_virus = false;

            DirectoryInfo dir = new DirectoryInfo(path);

                if (!Program.fm1.backgroundWorker1.CancellationPending)
                {
                    Node_Write.Virus? virus;
                    string signHash, signHash2;

                    //Program.fm1.progressbar_vivod();

                    //if (f == Path_Last_Deleted)
                    //{
                    //    Console.WriteLine("Найдена ошибка. Похоже файл удален. Пропускаю его");
                    //    continue;
                    //}        
                    foreach (string f2 in Directory.GetFiles(path, "*.*", SearchOption.AllDirectories).Union(Directory.GetFiles(path, "*.dll", SearchOption.AllDirectories)))
                    {
                        if (f == f2)
                        { proverka = true; break; }
                    }
                    if (!proverka)
                    {
                        Console.WriteLine("Похоже файл удален. Пропускаю его");
                        proverka = false;
                        return false;
                    }
                    proverka = false;

                    // Открываем файл
                    FileStream file = new FileStream(f, FileMode.Open);
                    // узнаем, где у него исп секция
                    int[] data = aS.exeSect(file);
                    // смещаем его на начало исп секции
                    file.Seek(data[1], SeekOrigin.Begin);

                    // прогоняем для всей исп области, смещаясь
                    // каждый раз на 1 байт и беря 8 байт как длину
                    // возможного вируса
                    while (file.Position + 8 < data[1] + data[0])
                    {
                        // получаем блок кода из сканируемого файла
                        // на месте возможного вируса длиной 8 байт
                        signHash = aS.read(file, 8);
                        // хешируем его
                        signHash = aS.hash(signHash);
                        // сравниваем с помощью АВЛ по хешу
                        // если возвращаемое значение не null, то
                        // это, возможно, зараженный файл и нужно
                        // проверить полный хеш
                        virus = tree.Find(signHash);
                        if (virus != null)
                        {
                            // возвращаемся назад на 8 байт
                            file.Seek(-8, SeekOrigin.Current);
                            // читаем длину сигнатуры
                            signHash2 = aS.read(file, ((Node_Write.Virus)virus).length);
                            signHash2 = aS.hash(signHash2);
                            // если полные хеши равны, то это точно зараженный файл
                            if (signHash2 == ((Node_Write.Virus)virus).hash)
                            {
                                Console.WriteLine("\nАнтивирус: Обнаруженена угроза.");
                                this.Observer_str.Add(f);
                                Observer_str2 = f;
                                this.Notify();

                                // добавляем в str путь к файлу
                                B_virus = true;
                                // ищем сигнатуру - источник заражения и
                                // прибавляем ей число обнаружений
                                for (int i = 0; i < sign.Count; i++)
                                {
                                    if (sign[i].hash == ((Node_Write.Virus)virus).hash)
                                    {
                                        //так делается, ибо это ошибка компилятора
                                        Node_Write.Virus vir = sign[i];
                                        vir.detect++;
                                        sign[i] = vir;
                                    }
                                }
                                break;
                            }
                            // отменяем смещения
                            file.Seek(8 - ((Node_Write.Virus)virus).length, SeekOrigin.Current);
                        }
                        // идем назад на 8 байт и смещаем на 1 байт
                        file.Seek(-8 + 1, SeekOrigin.Current);
                    }
                    file.Close();
            }

            // Путь к файлу базы сигнатур
            StreamReader sr = new StreamReader("sign.txt");
            string line = sr.ReadLine();
            sr.Close();

            // сериализуем структуру (превращаем в бинарную форму)
            FileStream VDB = new FileStream(line, FileMode.Create, FileAccess.Write);
            BinaryFormatter binForm = new BinaryFormatter();
            // добавляем в VDB (файл базы сигнатур) объект sign
            // оно добавится туда в 2ой форме и сложно будет что-то понять
            // чтобы что-то понять, надо будет десериализовывать



            for (int i = 0; i < sign.Count; i++)
            {
                binForm.Serialize(VDB, sign[i]);
            }
            VDB.Close();

            return B_virus;
        }



  
    }
}
