﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace AntiVirus
{
    public partial class work_File
    {
        /// <summary>
        /// Получить список сигнатур
        /// </summary>
        /// <returns>Список сигнатур</returns>
        public List<Node_Write.Virus> getVirusDB()
        {
            Form1 f1 = new Form1();
            BinaryFormatter binForm = new BinaryFormatter();

            // узнать путь к файлу
            StreamReader sr = new StreamReader("sign.txt");
            string line = sr.ReadLine();
            sr.Close();

            FileStream virusDataBase = new FileStream(line, FileMode.Open);
            List<Node_Write.Virus> dataBase = new List<Node_Write.Virus>();

            while (virusDataBase.Position < virusDataBase.Length)
            {
                dataBase.Add((Node_Write.Virus)binForm.Deserialize(virusDataBase));    
            }
            virusDataBase.Close();

            return dataBase;
        }

    }
}
