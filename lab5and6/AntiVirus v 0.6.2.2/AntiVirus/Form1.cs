﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using Microsoft.Win32;

namespace AntiVirus
{

    public partial class Form1 : Form
    {
        ScanEngine wFile = new ScanEngine();
        Node_Write NW = new Node_Write();
        List<string> InfFile = new List<string>();

        BackgroundWorker bw;
        BackgroundWorker bw2;

        public DateTime timer_nach;
        bool btimer = true;

        Observer_Virus_FileWatcher Observer_FileWatcher = new Observer_Virus_FileWatcher();
        public Form1()
        {
            InitializeComponent();
            panel3.Visible = false;
            panel_scan.Visible = false;
            tracking_panel.Visible = false;
            panelleft.Height =  home.Height;
            panelleft.Top =     home.Top;
            this.BackColor = Color.FromArgb(41, 44, 51); 
            this.MouseDown += new MouseEventHandler(Form1_MouseDown);
            paths();
        }

        void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            base.Capture = false;
            Message m = Message.Create(base.Handle, 0xa1, new IntPtr(2), IntPtr.Zero);
            this.WndProc(ref m);
        }


        /// <summary>
        /// Вывод последних данных в окошки
        /// при загрузке окна
        /// </summary>
        public void paths()
        {
            String line;
            // Последнее сканирование
            try
            {
                StreamReader sr = new StreamReader("date.txt");
                line = sr.ReadLine();
                scan_label.Text = "Последнее сканирование: " + line;
                sr.Close();

            }
            catch (Exception e)
            {
                scan_label.Text = e.Message;
            }

            // Путь сканирования
            try
            {
                StreamReader sr = new StreamReader("path.txt");
                line = sr.ReadLine();
                scanPath.Text = "Путь сканирования: " + line;
                sr.Close();
                //IFormatter formatter = new BinaryFormatter();
                //FileStream buffer = File.OpenRead("path.txt");
                //Virus line = formatter.Deserialize(buffer) as Virus;
                //buffer.Close();
            }
            catch (Exception e)
            {
                scanPath.Text = e.Message;
            }

            //Путь базы вирусов
            try
            {
                StreamReader sr = new StreamReader("sign.txt");
                line = sr.ReadLine();
                signPath.Text = "Путь к файлу базы вирусов: " + line;
                sr.Close();
            }
            catch (Exception e)
            {
                signPath.Text = e.Message;
            }

            //Путь FileWatcher
            try
            {
                StreamReader sr = new StreamReader("filewatcher.txt");
                line = sr.ReadLine();
                file_watcher_textbox.Text = "Путь к директории наблюдения: " + line;
                sr.Close();
            }
            catch (Exception e)
            {
                file_watcher_textbox.Text = e.Message;
            }
            // это не помогает
            Refresh();
        }

        #region Other

        private void Close2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion Other

       
        /// <summary>
        /// +Путь сканирования
        /// </summary>
        private void DirScan_Click(object sender, EventArgs e)
        {
            // Путь сканирования
            String line = wFile.pathScan();
            scanPath.Text = "Путь сканирования: " + line;

            // Вывести все pe файлы директории в listbox
            DirectoryInfo dir = new DirectoryInfo(line);
            listBox1.Items.Clear();
            files.Text = "PE файлы директории";
            foreach (string f in Directory.GetFiles(line, "*.*", SearchOption.AllDirectories).Union(Directory.GetFiles(line, "*.dll", SearchOption.AllDirectories)))
            {
                string[] splitpath = f.Split('\\');
                string name = splitpath[splitpath.Length - 1];
                scan_listBox.Items.Add(name);
            }
            scan_label_2.Text = "Кол-во файлов в данной дириктории: " + scan_listBox.Items.Count;
        }

        /// <summary>
        /// +Путь к файлу базы вирусов
        /// </summary>
        private void changeSignPath_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog OBD = new System.Windows.Forms.OpenFileDialog();
            if (OBD.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    StreamWriter sw = new StreamWriter("sign.txt");
                    sw.WriteLine(OBD.FileName);
                    sw.Close();
                }
                catch (Exception err)
                {
                    System.Windows.MessageBox.Show("Exception: " + err.Message);
                }
                signPath.Text = "Путь к файлу базы вирусов: " + OBD.FileName;
            }
        }

        public void Print_scan_listBox(String line)
        {
            if (scan_listBox.InvokeRequired)
            {
                //scan_listBox.Invoke(new Action(() => scan_listBox.Items.Clear()));
                this.Invoke(new System.Threading.ThreadStart(delegate
                {
                    scanir.Enabled = true;
                }));
                    for (int j = 0; j < InfFile.Count; j++)
                    {
                        line = InfFile[j];
                        //scan_listBox.Invoke(new Action(() => scan_listBox.Items.Add(line)));
                        listbox_scan_listBox(line);
                    }
                    //this.Invoke(new System.Threading.ThreadStart(delegate
                    //{
                    //    progressBar1.Value = 0;
                    //    bunifuCircleProgressbar1.Value = 0;
                    //}));
            }
        }

       // void func_timer()
        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            DateTime time;
            TimeSpan value;
            string date;
            while (btimer)
            {
                this.Invoke(new System.Threading.ThreadStart(delegate
                {
                    time = DateTime.Now;
                    value = time.Subtract(timer_nach);
                    date = value.ToString().Substring(0, 8);
                    scan_label_2.Text = String.Format("Время сканирования {0}", date);
                }));
            }          
        }
               
        //void Potok1(object sender, EventArgs e)
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {         
            String line = "";
            StreamReader sr = new StreamReader("path.txt");
            line = sr.ReadLine();
            sr.Close();          

            var subject = new ScanEngine();
            var observerA = new Observer_Virus_detection();
            subject.Attach(observerA);
         
            this.Invoke(new System.Threading.ThreadStart(delegate
            {
                scanir.Enabled = false;
                //progressBar1.Style = ProgressBarStyle.Marquee;
                timer_nach = DateTime.Now;
                btimer = true;
            }));

            //bw2 = new BackgroundWorker();
            //bw2.DoWork += (obj, ea) => filewatcher();
            //bw2.RunWorkerAsync();

            //Thread potok2 = new Thread(() => filewatcher());
            //potok2.IsBackground = true;
            //potok2.Start();

            if (!backgroundWorker2.IsBusy)
            {
                backgroundWorker2.RunWorkerAsync();
            }

           // Preparation_scan pr = new Preparation_scan();
            //pr.preparation(line);

            InfFile = subject.scan_List(line);

            this.Invoke(new System.Threading.ThreadStart(delegate
            {
                //progressBar1.Style = ProgressBarStyle.Blocks;
                btimer = false;
            }));

            subject.Detach(observerA);
       
            Print_scan_listBox(line);
            this.Invoke(new System.Threading.ThreadStart(delegate
            {
                progressBar1.Value = 0;
                bunifuCircleProgressbar1.Value = 0;
            }));
        }


        /// <summary>
        ///+ Сканировать
        /// </summary>
        private void scanir_Click_1(object sender, EventArgs e)
        {     
            // Обновить последнее сканирование
            try
            {
                StreamWriter sw = new StreamWriter("date.txt");
                sw.WriteLine(DateTime.Now);
                sw.Close();
            }
            catch (Exception err)
            {
                System.Windows.MessageBox.Show("Exception: " + err.Message);
            }
            scan_label.Text = "Последнее сканирование: " + DateTime.Now;
  
            //Thread.Sleep(20);
            // Считываем путь, по которому надо искать
            String line = "";
            StreamReader sr = new StreamReader("path.txt");
            line = sr.ReadLine();
            sr.Close();
            //bw = new BackgroundWorker();
            //bw.DoWork += (obj, ea) => Potok1(sender,e);
            //bw.RunWorkerAsync();

            int maxfile = 0;
            foreach (string f in Directory.GetFiles(line, "*.*", SearchOption.AllDirectories).Union(Directory.GetFiles(line, "*.dll", SearchOption.AllDirectories)))
            {
                maxfile++;
            }
            progressBar1.Minimum = 0;
            progressBar1.Maximum = maxfile;
            progressBar1.Value = 0;
            progressBar1.Step = 1;

            bunifuCircleProgressbar1.MaxValue = maxfile;
            bunifuCircleProgressbar1.Value = 0;
            
            scan_listBox.Invoke(new Action(() => scan_listBox.Items.Clear()));
            if (!backgroundWorker1.IsBusy)
            {
                backgroundWorker1.RunWorkerAsync();
            }        
        }

        /// <summary>
        ///+ Добавить сигнатуру
        /// </summary>
        private void addSign_Click(object sender, EventArgs e)
        {
            addSign addSign = new addSign();
            addSign.Show();
        }

        /// <summary>
        /// +вывести список сигнатур
        /// </summary>
        private void Basesign_Click(object sender, EventArgs e)
        {      
            NW.Signatures_List(listBox1, files);           
        }

        /// <summary>
        /// +Удалить зараженный файл
        /// </summary>
        private void deleteFile_Click_2(object sender, EventArgs e)
        {
            // ищем, что было выделено в listbox1
            for (int i = 0; i < scan_listBox.Items.Count; i++)
            {
                if (scan_listBox.Items[i] == scan_listBox.SelectedItem)
                {
                    // получаем путь к файлу через его структуру
                    string Path = scan_listBox.Items[i].ToString();
                   // Path = Path.Substring(6);
                    // удаляем файл
                    File.Delete(Path);

                    // после удаления файла надо удалить 
                    // запись о файле в listBox
                    scan_listBox.Items.RemoveAt(i);

                    break;
                }
            }
        }

        /// <summary>
        /// +Удалить сигнатуру из файла
        /// </summary>
        private void deleteInfFile_Click(object sender, EventArgs e)
        {
            List<Node_Write.Virus> buffer = new List<Node_Write.Virus>();

            // ищем, что было выделено в listbox1
            for (int i = 0; i < listBox1.Items.Count; i++)
            {
                if (listBox1.Items[i] == listBox1.SelectedItem)
                {
                    StreamReader sr = new StreamReader("sign.txt");
                    string line = sr.ReadLine();
                    sr.Close();

                    FileStream InfectedFilesDB = new FileStream(line, FileMode.Open, FileAccess.Read);
                    BinaryFormatter binForm = new BinaryFormatter();

                    for (int k = 0; k < listBox1.Items.Count; k++)
                    {
                        Node_Write.Virus inf = (Node_Write.Virus)binForm.Deserialize(InfectedFilesDB);
                        if (k != i)
                        {
                            buffer.Add(inf);
                        }
                    }
                    InfectedFilesDB.Close();

                    // сериализуем buffer
                    InfectedFilesDB = new FileStream(line, FileMode.Create);
                    for (int j = 0; j < buffer.Count; j++)
                    {
                        binForm.Serialize(InfectedFilesDB, buffer[j]);
                    }

                    // надо удалить запись о сигнатурев listBox
                    listBox1.Items.RemoveAt(i);

                    InfectedFilesDB.Close();
                    break;
                }
            }
        }

        #region Page Button
        private void home_Click(object sender, EventArgs e)
        {

            panel2.Visible = true;
            panel3.Visible = false;
            panel_scan.Visible = false;
            tracking_panel.Visible = false;

            panelleft.Top = home.Top;
            panelleft.Height = home.Height;
        }

        private void Inform_Click(object sender, EventArgs e)
        {
            panel3.Visible = true;
            panel2.Visible = false;
            panel_scan.Visible = false;
            tracking_panel.Visible = false;

            panelleft.Top = Inform.Top;
            panelleft.Height = Inform.Height;
        }

        private void main_scan_Click(object sender, EventArgs e)
        {
            panel2.Visible = false;
            panel3.Visible = false;
            panel_scan.Visible = true;
            tracking_panel.Visible = false;

            panelleft.Top = main_scan.Top;
            panelleft.Height = main_scan.Height;

        }

        private void tracking_Click(object sender, EventArgs e)
        {
            panel2.Visible = false;
            panel3.Visible = false;
            panel_scan.Visible = false;
            tracking_panel.Visible = true;

            panelleft.Top = tracking.Top;
            panelleft.Height = tracking.Height;
        }

        #endregion Page Button
        //  mybutton.Location = new Point(X, Y);

        public void listbox_scan_listBox(string str)
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    scan_listBox.Items.Add(str);
                }));
            }
        }
        public void progressbar_sbros()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    progressBar1.Value = 0;
                }));
            }           
        }
        public void progressbar_zapol(string line)
        {
            int maxfile = 0;
            foreach (string f in Directory.GetFiles(line, "*.*", SearchOption.AllDirectories).Union(Directory.GetFiles(line, "*.dll", SearchOption.AllDirectories)))
            {
                maxfile++;
            }
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    progressBar1.Minimum = 1;
                    progressBar1.Maximum = maxfile;
                    progressBar1.Value = 0;
                    progressBar1.Step = 1;
                }));
            }
        }

        public void progressbar_vivod()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    progressBar1.PerformStep();
                    bunifuCircleProgressbar1.Value++;
                }));
            }
        }

        void Proverka_Registry()
        {
            RegistryKey key_create = Registry.CurrentUser.CreateSubKey(@"Software\Antivirus");
            RegistryKey key_open = Registry.CurrentUser.OpenSubKey(@"Software\Antivirus");


            foreach (string name in key_open.GetValueNames())
            {
                Console.WriteLine(name);
                //switch (name)
                //{
                //    case "sign":
                //        break;
                //    case "path":
                //        break;
                //    case "date":
                //        break;
                //    case "base":
                //        break;
                //    default:
                //        break;
                //}

            }

            //Установить значние в параметр с словом значение
            //key_create.SetValue("Параметр", "Значение", RegistryValueKind.String);


            //string name = key.GetValue("параметр").ToString();         
        }
        //  file_watcher_textbox    textbox
        //  file_watcher_listBox    listbox
        //Путь отслеживания
        private void file_watcher_path_Click(object sender, EventArgs e)
        {
            // Путь сканирования

            String line = "";
            FolderBrowserDialog FBD = new FolderBrowserDialog();
            if (FBD.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    StreamWriter sw = new StreamWriter("filewatcher.txt");
                    sw.WriteLine(FBD.SelectedPath);
                    sw.Close();
                }
                catch (Exception err)
                {
                    System.Windows.MessageBox.Show("Exception: " + err.Message);
                }

                line = FBD.SelectedPath;
            }

            file_watcher_textbox.Text = "Путь к директории наблюдения: " + line;

            // Вывести все pe файлы директории в listbox
            DirectoryInfo dir = new DirectoryInfo(line);

            file_watcher_listBox.Items.Clear();

            foreach (string f in Directory.GetFiles(line, "*.*", SearchOption.AllDirectories).Union(Directory.GetFiles(line, "*.dll", SearchOption.AllDirectories)))
            {
                string[] splitpath = f.Split('\\');
                string name = splitpath[splitpath.Length - 1];
                file_watcher_listBox.Items.Add(name);
            }
            file_watcher_label.Text = "Кол-во файлов в данной директории: " + file_watcher_listBox.Items.Count;
        }

        //Начать слежение
        FileWatcher watcher = new FileWatcher();
        private void file_watcher_start_Click(object sender, EventArgs e)
        {
            watcher.Attach(Observer_FileWatcher);
            file_watcher_start.Enabled = false;
            watcher.FileMonitor_Start();
        }
        //Отменить слежение
        private void file_watcher_stop_Click(object sender, EventArgs e)
        {
            watcher.Detach(Observer_FileWatcher);
            watcher.FileMonitor_Stop();
            file_watcher_start.Enabled = true;
        }
        private void file_watcher_clear_Click(object sender, EventArgs e)
        {
            file_watcher_listBox.Items.Clear();
        }

        public void Filewatcher_listbox(string str)
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    file_watcher_listBox.Items.Add(str);
                }));
            }
        }
        public void Filewatcher_listbox_Clear()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    file_watcher_listBox.Items.Clear();
                }));
            }
        }

        public void ProgressBar_MaxValue(int maxfile)
        {
            this.Invoke(new System.Threading.ThreadStart(delegate
            {
                Program.fm1.progressBar1.Maximum = maxfile;
                Program.fm1.bunifuCircleProgressbar1.MaxValue = maxfile;
            }));
        }


    }

}

