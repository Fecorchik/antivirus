﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AntiVirus
{
    public interface IObserver
    {
        // Получает обновление от издателя
        void Update(ISubject subject);
    }

    public interface ISubject
    {
        // Присоединяет наблюдателя к издателю.
        void Attach(IObserver observer);

        // Отсоединяет наблюдателя от издателя.
        void Detach(IObserver observer);

        // Уведомляет всех наблюдателей о событии.
        void Notify();
    }
    //Обнаружение вирусов
    class Observer_Virus_detection : IObserver
    {
        public async void Update(ISubject subject)
        {

            if ((subject as ScanEngine).Observer_str.Count > 0)
            {
                Console.WriteLine("Наблюдатель: Обнаружен вирус, передаю путь до него" + "\n" + (subject as ScanEngine).Observer_str2);
                Program.fm1.Print_scan_listBox((subject as ScanEngine).Observer_str2);
            }
        }
    }

    //Слежение за изменениями в папке
    class Observer_Virus_FileWatcher : IObserver
    {
        public async void Update(ISubject subject)
        {
            string line;
            if ((subject as FileWatcher).BoolPath_Create)
            {
               // line = "Observer_FileWatcher: Обнаружен новый файл, передаю его сканеру:" + "\n" + (subject as FileWatcher).FullPath_Create.ToString();
                line = String.Format("Observer_FileWatcher: Обнаружен новый файл, передаю его сканеру:{0}", (subject as FileWatcher).FullPath_Create.ToString());
                Program.fm1.Filewatcher_listbox(line);

                ScanEngine wf = new ScanEngine();
                bool virus = false;
                virus = wf.scan_String((subject as FileWatcher).Put, (subject as FileWatcher).FullPath_Create.ToString());

                if (virus)
                {
                    line = String.Format("Observer_FileWatcher: Новый файл является вирусом! {0}", (subject as FileWatcher).FullPath_Create.ToString());
                    Program.fm1.Filewatcher_listbox(line);
                }
                else
                {
                    line = String.Format("Observer_FileWatcher: Обнаруженный файл не является вирусом {0}", (subject as FileWatcher).FullPath_Create.ToString());
                    Program.fm1.Filewatcher_listbox(line);
                }
                
            }

            if ((subject as FileWatcher).BoolPath_Deleted)
            {
                line = String.Format("Observer_FileWatcher: Удален файл! {0}",(subject as FileWatcher).FullPath_Deleted.ToString());
                Program.fm1.Filewatcher_listbox(line);
            }
        }
    }


}
