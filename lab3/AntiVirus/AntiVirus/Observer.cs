﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AntiVirus
{
    public interface IObserver
    {
        // Получает обновление от издателя
        void Update(ISubject subject);
    }

    public interface ISubject
    {
        // Присоединяет наблюдателя к издателю.
        void Attach(IObserver observer);

        // Отсоединяет наблюдателя от издателя.
        void Detach(IObserver observer);

        // Уведомляет всех наблюдателей о событии.
        void Notify();
    }

    class ConcreteObserverA : IObserver
    {
        public async void Update(ISubject subject)
        {
            if ((subject as work_File).Observer_str.Count > 0)
            {
                Console.WriteLine("Обнаружен вирус!");
            }
        }
    }

}
