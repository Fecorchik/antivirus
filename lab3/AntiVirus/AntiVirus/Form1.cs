﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using Microsoft.Win32;

namespace AntiVirus
{

    public partial class Form1 : Form
    {
        work_File wFile = new work_File();
        Node_Write NW = new Node_Write();
        List<string> InfFile = new List<string>();

        BackgroundWorker bw;
        BackgroundWorker bw2;

        public DateTime timer_nach;
        bool btimer = true;

        public Form1()
        {
            InitializeComponent();
            panel3.Visible = false;
            panel_scan.Visible = false;
            panelleft.Height =  home.Height;
            panelleft.Top =     home.Top;
            this.BackColor = Color.FromArgb(41, 44, 51); 
            this.MouseDown += new MouseEventHandler(Form1_MouseDown);
            paths();
        }

        void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            base.Capture = false;
            Message m = Message.Create(base.Handle, 0xa1, new IntPtr(2), IntPtr.Zero);
            this.WndProc(ref m);
        }


        /// <summary>
        /// Вывод последних данных в окошки
        /// при загрузке окна
        /// </summary>
        public void paths()
        {
            String line;
            // Последнее сканирование
            try
            {
                StreamReader sr = new StreamReader("date.txt");
                line = sr.ReadLine();
                scan_label.Text = "Последнее сканирование: " + line;
                sr.Close();

            }
            catch (Exception e)
            {
                scan_label.Text = e.Message;
            }

            // Путь сканирования
            try
            {
                StreamReader sr = new StreamReader("path.txt");
                line = sr.ReadLine();
                scanPath.Text = "Путь сканирования: " + line;
                sr.Close();
                //IFormatter formatter = new BinaryFormatter();
                //FileStream buffer = File.OpenRead("path.txt");
                //Virus line = formatter.Deserialize(buffer) as Virus;
                //buffer.Close();
            }
            catch (Exception e)
            {
                scanPath.Text = e.Message;
            }

            //Путь базы вирусов
            try
            {
                StreamReader sr = new StreamReader("sign.txt");
                line = sr.ReadLine();
                signPath.Text = "Путь к файлу базы вирусов: " + line;
                sr.Close();
            }
            catch (Exception e)
            {
                signPath.Text = e.Message;
            }
            // это не помогает
            Refresh();
        }

        #region Other

        private void Close2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion Other

       
        /// <summary>
        /// +Путь сканирования
        /// </summary>
        private void DirScan_Click(object sender, EventArgs e)
        {

            // Путь сканирования
            String line = wFile.pathScan();
            scanPath.Text = "Путь сканирования: " + line;

            // Вывести все pe файлы директории в listbox
            DirectoryInfo dir = new DirectoryInfo(line);
            listBox1.Items.Clear();
            files.Text = "PE файлы директории";
            foreach (string f in Directory.GetFiles(line, "*.*", SearchOption.AllDirectories).Union(Directory.GetFiles(line, "*.dll", SearchOption.AllDirectories)))
            {
                string[] splitpath = f.Split('\\');
                string name = splitpath[splitpath.Length - 1];
                scan_listBox.Items.Add(name);
            }
            scan_label_2.Text = "Кол-во файлов в данной дириктории: " + scan_listBox.Items.Count;
        }

        /// <summary>
        /// +Путь к файлу базы вирусов
        /// </summary>
        private void changeSignPath_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog OBD = new System.Windows.Forms.OpenFileDialog();
            if (OBD.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    StreamWriter sw = new StreamWriter("sign.txt");
                    sw.WriteLine(OBD.FileName);
                    sw.Close();
                }
                catch (Exception err)
                {
                    System.Windows.MessageBox.Show("Exception: " + err.Message);
                }
                signPath.Text = "Путь к файлу базы вирусов: " + OBD.FileName;
            }
        }

        void Print_Listbox1(String line)
        {
            if (listBox1.InvokeRequired)
            {
                scan_listBox.Invoke(new Action(() => scan_listBox.Items.Clear()));
                this.Invoke(new System.Threading.ThreadStart(delegate
                {
                    scanir.Enabled = true;
                }));
                    for (int j = 0; j < InfFile.Count; j++)
                    {
                        line = InfFile[j];
                        scan_listBox.Invoke(new Action(() => scan_listBox.Items.Add(line)));
                    }
            }
        }

       // void func_timer()
        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            DateTime time;
            TimeSpan value;
            string date;
            while (btimer)
            {
                this.Invoke(new System.Threading.ThreadStart(delegate
                {
                    time = DateTime.Now;
                    value = time.Subtract(timer_nach);
                    date = value.ToString().Substring(0, 8);
                    scan_label_2.Text = String.Format("Время сканирования {0}", date);
                }));
            }
            
        }
              
        //void Potok1(object sender, EventArgs e)
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {         
            String line = "";
            StreamReader sr = new StreamReader("path.txt");
            line = sr.ReadLine();
            sr.Close();          
            var subject = new work_File();
            var observerA = new ConcreteObserverA();
            subject.Attach(observerA);

            this.Invoke(new System.Threading.ThreadStart(delegate
            {
                scanir.Enabled = false;
                progressBar1.Style = ProgressBarStyle.Marquee;
                timer_nach = DateTime.Now;
                btimer = true;
            }));

            //bw2 = new BackgroundWorker();
            //bw2.DoWork += (obj, ea) => func_timer();
            //bw2.RunWorkerAsync();

            if (!backgroundWorker2.IsBusy)
            {
                backgroundWorker2.RunWorkerAsync();
            }

            InfFile = subject.scan(line);

            this.Invoke(new System.Threading.ThreadStart(delegate
            {
                progressBar1.Style = ProgressBarStyle.Blocks;
                btimer = false;
            }));

                Print_Listbox1(line);
        }


        /// <summary>
        ///+ Сканировать
        /// </summary>
        private void scanir_Click_1(object sender, EventArgs e)
        {
            
            // Обновить последнее сканирование
            try
            {
                StreamWriter sw = new StreamWriter("date.txt");
                sw.WriteLine(DateTime.Now);
                sw.Close();
            }
            catch (Exception err)
            {
                System.Windows.MessageBox.Show("Exception: " + err.Message);
            }
            scan_label.Text = "Последнее сканирование: " + DateTime.Now;
  
            //Thread.Sleep(20);
            // Считываем путь, по которому надо искать
            String line = "";
            StreamReader sr = new StreamReader("path.txt");
            line = sr.ReadLine();
            sr.Close();

            //listBox1.Items.Clear();

            //int maxfile = 0;
            //foreach (string f in Directory.GetFiles(line, "*.*", SearchOption.AllDirectories).Union(Directory.GetFiles(line, "*.dll", SearchOption.AllDirectories)))
            //{
            //    maxfile++;
            //}

            //progressBar1.Minimum = 0;
            //progressBar1.Maximum = maxfile;
            //progressBar1.Step = 1;
            //progressBar1.Value = 0;

            
            //SynchronizationContext uiContext = SynchronizationContext.Current;

            //bw = new BackgroundWorker();
            //bw.DoWork += (obj, ea) => Potok1(sender,e);
            //bw.RunWorkerAsync();

            if (!backgroundWorker1.IsBusy)
            {
                backgroundWorker1.RunWorkerAsync();
            }

           // Thread potok1 = new Thread(Potok1);
            //Thread potok1 = new Thread(() => Potok1(progressBar1));
            //potok1.IsBackground = true;
            //potok1.Start();          
          
        }




        /// <summary>
        ///+ Добавить сигнатуру
        /// </summary>
        private void addSign_Click(object sender, EventArgs e)
        {
            addSign addSign = new addSign();
            addSign.Show();
        }

        /// <summary>
        /// +вывести список сигнатур
        /// </summary>
        private void Basesign_Click(object sender, EventArgs e)
        {      
            NW.Signatures_List(listBox1, files);           
        }

        /// <summary>
        /// +Удалить зараженный файл
        /// </summary>
        private void deleteFile_Click_2(object sender, EventArgs e)
        {
            // ищем, что было выделено в listbox1
            for (int i = 0; i < scan_listBox.Items.Count; i++)
            {
                if (scan_listBox.Items[i] == scan_listBox.SelectedItem)
                {
                    // получаем путь к файлу через его структуру
                    string Path = Convert.ToString(listBox1.Items[i]);
                   // Path = Path.Substring(6);
                    // удаляем файл
                    File.Delete(Path);

                    // после удаления файла надо удалить 
                    // запись о файле в listBox
                    listBox1.Items.RemoveAt(i);

                    break;
                }
            }
        }

        /// <summary>
        /// +Удалить сигнатуру из файла
        /// </summary>
        private void deleteInfFile_Click(object sender, EventArgs e)
        {
            List<Node_Write.Virus> buffer = new List<Node_Write.Virus>();

            // ищем, что было выделено в listbox1
            for (int i = 0; i < listBox1.Items.Count; i++)
            {
                if (listBox1.Items[i] == listBox1.SelectedItem)
                {
                    StreamReader sr = new StreamReader("sign.txt");
                    string line = sr.ReadLine();
                    sr.Close();

                    FileStream InfectedFilesDB = new FileStream(line, FileMode.Open, FileAccess.Read);
                    BinaryFormatter binForm = new BinaryFormatter();

                    for (int k = 0; k < listBox1.Items.Count; k++)
                    {
                        Node_Write.Virus inf = (Node_Write.Virus)binForm.Deserialize(InfectedFilesDB);
                        if (k != i)
                        {
                            buffer.Add(inf);
                        }
                    }
                    InfectedFilesDB.Close();

                    // сериализуем buffer
                    InfectedFilesDB = new FileStream(line, FileMode.Create);
                    for (int j = 0; j < buffer.Count; j++)
                    {
                        binForm.Serialize(InfectedFilesDB, buffer[j]);
                    }

                    // надо удалить запись о сигнатурев listBox
                    listBox1.Items.RemoveAt(i);

                    InfectedFilesDB.Close();
                    break;
                }
            }
        }

        #region Page Button
        private void home_Click(object sender, EventArgs e)
        {

            panel2.Visible = true;
            panel3.Visible = false;
            panel_scan.Visible = false;

            panelleft.Top = home.Top;
            panelleft.Height = home.Height;
        }

        private void Inform_Click(object sender, EventArgs e)
        {
            panel3.Visible = true;
            panel2.Visible = false;
            panel_scan.Visible = false;

            panelleft.Top = Inform.Top;
            panelleft.Height = Inform.Height;
        }

        private void main_scan_Click(object sender, EventArgs e)
        {
            panel2.Visible = false;
            panel3.Visible = false;
            panel_scan.Visible = true;

            panelleft.Top = main_scan.Top;
            panelleft.Height = main_scan.Height;

        }

        #endregion Page Button
        //  mybutton.Location = new Point(X, Y);
    }

}

